$(function () {
    var form = layui.form
    form.verify({
        nickname: function (value) {
            if (value.length > 6) {
                return '昵称长度必须在 1 ~ 6 个字符之间！'
            }
        }
    })

    initUserInfo();
    // 初始化用户的基本信息
    function initUserInfo() {
        $.ajax({
            type: "GET",
            url: "/my/userinfo",
            success: function (response) {
                if (response.status !== 0) {
                    // status--类型int  0代表成功
                    return layer.msg('获取用户信息失败');
                }
                console.log(response);
                form.val('formUserInfo', response.data)
            }
        });
    }

    // 重置表单数据
    $('#btnReset').on('click', function (e) {
        // 阻止表单的默认重置行为
        e.preventDefault();
        initUserInfo();
    });

    // 监听表单的提交事件
    // layui-form是隐藏域上面的class类名
    $('layui-form').on('submit', function (e) {
        // 阻止表单的默认重置行为
        e.preventDefault();
        // 发起ajax数据请求
        $.ajax({
            type: "POST",
            url: "/my/userinfo",
            // 这里的this是代表什么？
            // 指的就是
            data: $(this).serialize(),
            dataType: "dataType",
            success: function (response) {
                if (response.status !== 0) {
                    return layer.msg('更新用户信息失败！')
                }
                layer.msg('更新用户信息成功！')
                // 调用父页面中的方法，重新渲染用户的头像和用户的信息
                window.parent.getUserInfo();
            }
        });
    });
})