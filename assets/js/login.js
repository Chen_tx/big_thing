$(function () {
    // 鼠标点击注册页面
    $('#link_res').on('click', function () {
        $('.loginAddress').hide();
        $('.resAddress').show();
    });
    // 鼠标点击登录页面
    $('#link_login').on('click', function () {
        $('.loginAddress').show();
        $('.resAddress').hide();
    });

    // 表单验证
    let form = layui.form;
    let layer = layui.layer
    form.verify({
        /*username: function (value, item) { //value：表单的值、item：表单的DOM对象
            if (!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)) {
                return '用户名不能有特殊字符';
            }
            if (/(^\_)|(\__)|(\_+$)/.test(value)) {
                return '用户名首尾不能出现下划线\'_\'';
            }
            if (/^\d+\d+\d$/.test(value)) {
                return '用户名不能全为数字';
            }
        },
        自定义了一个叫做 pwd 校验规则
        pwd: [/^[\S]{6,12}$/, '密码必须6到12位，且不能出现空格'],
        */
        // 校验两次密码是否一致的规则
        repwd: function (value) {
            // 通过形参拿到的是确认密码框中的内容
            // 还需要拿到密码框中的内容
            // 然后进行一次等于的判断
            // 如果判断失败,则return一个提示消息即可
            var valpwd = $('.resAddress [name=password]').val()
            if (valpwd !== value) {
                return '两次密码不一致'
            }
        }
    });

    /*注册事件(原版)
    $('#form_reg').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            type: "post",
            url: 'http://www.liulongbin.top:3007/api/reguser',
            data: {
                // username: $('#regusername'),
                // password: $('#regpass')
                // 注册的时候记得写 val() 不然收集不到
                username: $('#regusername').val(),
                password: $('#regpass').val()
            },
            success: function (response) {
                console.log(response);
            }
        });
        if (response.status != 0) {
            layer.msg('注册失败');
        }
        layer.msg('注册成功！请登录')
        // 手动触发登录
        $('#link_login').click();

    })
    */

    // 注册事件
    $('#form_reg').on('submit', function (e) {
        e.preventDefault()
        var data = {
            username: $('#form_reg [name=username]').val(),
            password: $('#form_reg [name=password]').val()
        }
        $.post("/api/reguser", data, function (res) {
            // $.post("http://www.liulongbin.top:3007/api/reguser", data, function (res) {
            console.log(res);
            if (res.status != 0) {
                return layer.msg(res.message)
            }
            layer.msg('注册成功，请登录！')
            $('#link_login').click()
        })
    })
    // 登录事件
    $('#form_login').submit(function (e) {
        // 阻止默认提交行为
        e.preventDefault()
        $.ajax({
            // url: 'http://www.liulongbin.top:3007/api/login',
            url: "/api/login",
            method: 'post',
            // 快速获取表单中的数据
            data: $(this).serialize(),
            success: function (res) {
                if (res.status !== 0) {
                    return layer.msg('登录失败！')
                }
                layer.msg('登录成功！')
                // 将登录成功得到的 token 字符串，保存到 localStorage 中
                localStorage.setItem('token', res.token)
                // 跳转到后台主页
                location.href = 'Big_index.html'
            }
        })
    })

})